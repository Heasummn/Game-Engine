#include "ImageLoader.h"
#include <Utils/errorutils.h>
#include <SOIL/SOIL.h>

using namespace engine::graphics;

Texture engine::ImageLoader::loadTexture(const std::string & filepath)
{
	Texture texture = {};

	texture.id = SOIL_load_OGL_texture(
		filepath.c_str(),
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y
		);

	if (texture.id == 0)
	{
		// Something failed in loading the texture
		engine::fatalError("SOIL Error while loading texture " + filepath + ": " + std::string(SOIL_last_result()));
	}

	// SOIL already made us a texture, so just bind it.
	glBindTexture(GL_TEXTURE_2D, texture.id);

	// Get the height
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &texture.height);
	// Get the width
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &texture.width);

	// Set a bunch of parameters to the texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Unbind the texture
	glBindTexture(GL_TEXTURE_2D, 0);

	return texture;
}
