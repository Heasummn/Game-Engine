#pragma once
#include <Graphics\Texture.h>
#include <string>

namespace engine
{
	// ImageLoader is a static class, so no ctor/dtor
	class ImageLoader
	{
	public:
		static graphics::Texture loadTexture(const std::string& filepath);
	};
}