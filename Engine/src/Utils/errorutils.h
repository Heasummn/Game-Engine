#pragma once
#include <string>
#include <iostream>
#include <SDL/SDL.h>

namespace engine
{
	inline void fatalError(const std::string& error)
	{
		std::cout << error << std::endl;
		std::cout << "Enter a key to quit... ";
		int tmp;
		std::cin >> tmp;
		SDL_Quit();
		exit(1);
	}
};