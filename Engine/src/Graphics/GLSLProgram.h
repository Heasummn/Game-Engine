#pragma once
#include <string>
#include <GL/glew.h>

class GLSLProgram
{
public:
	GLSLProgram();
	~GLSLProgram();

	void compileShaders(const std::string& vertexFilePath, const std::string& fragFilePath);
	void linkShaders();
	void use();
	void unuse();
	GLuint getUniformLocation(const std::string& name);
	void setUniform(const std::string& name, float value);
	void setUniform(const std::string& name, int value);

private:
	void compileShader(const std::string& filePath, GLuint& id);
	GLuint _programID, _vertexID, _fragmentID;
};

