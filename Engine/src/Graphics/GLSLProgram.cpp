#include "GLSLProgram.h"
#include "Utils/errorutils.h"
#include "Utils/fileutils.h"

GLSLProgram::GLSLProgram() : _programID(0), _vertexID(0), _fragmentID(0)
{}

GLSLProgram::~GLSLProgram()
{
}

void GLSLProgram::compileShaders(const std::string& vertexFilePath, const std::string& fragFilePath)
{
	_programID = glCreateProgram();
	_vertexID = glCreateShader(GL_VERTEX_SHADER);
	if (_vertexID == 0)
	{
		engine::fatalError("Vertex Shader failed to be created");
	}

	_fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
	if (_fragmentID == 0)
	{
		engine::fatalError("Fragment Shader failed to be created");
	}

	compileShader(vertexFilePath, _vertexID);
	compileShader(fragFilePath, _fragmentID);

}

void GLSLProgram::compileShader(const std::string& filePath, GLuint& id)
{
	std::string file = engine::readFile(filePath.c_str());
	if (file.empty())
		engine::fatalError("Failed to open file: " + filePath);

	const char* source = file.c_str();
	glShaderSource(id, 1, &source, nullptr);
	glCompileShader(id);

	GLchar infolog[1024];
	GLint success;
	glGetShaderiv(id, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(id, 1024, NULL, infolog);
		engine::fatalError((std::string) "Compiling Shader failed, " + infolog);

		glDeleteShader(id);
	}
}

void GLSLProgram::linkShaders()
{
	glAttachShader(_programID, _vertexID);
	glAttachShader(_programID, _fragmentID);

	glLinkProgram(_programID);

	GLint isLinked = 0;
	glGetProgramiv(_programID, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLchar infolog[1024];
		glGetProgramInfoLog(_programID, 1024, nullptr, infolog);

		glDeleteProgram(_programID);
		glDeleteShader(_vertexID);
		glDeleteShader(_fragmentID);

		engine::fatalError("Failed to Link Shader, " + (std::string) infolog);
	}

	glDetachShader(_programID, _vertexID);
	glDetachShader(_programID, _fragmentID);
	glDeleteShader(_vertexID);
	glDeleteShader(_fragmentID);
}

void GLSLProgram::use()
{
	glUseProgram(_programID);
}

void GLSLProgram::unuse()
{
	glUseProgram(0);
}

GLuint GLSLProgram::getUniformLocation(const std::string& name)
{
	GLint loc = glGetUniformLocation(_programID, name.c_str());
	if (loc == GL_INVALID_INDEX)
		engine::fatalError("Failed to get Uniform " + name + " from shader");

	return loc;
}

void GLSLProgram::setUniform(const std::string & name, float value)
{
	GLuint uniformLoc = getUniformLocation(name);
	glUniform1f(uniformLoc, value);
}

void GLSLProgram::setUniform(const std::string & name, int value)
{
	GLuint uniformLoc = getUniformLocation(name);
	glUniform1i(uniformLoc, value);
}
