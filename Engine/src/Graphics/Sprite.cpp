#include "Sprite.h"
#include <cstddef>

#include "Vertex.h"
using namespace engine::graphics;

Sprite::Sprite() : _vboID(0)
{
}


Sprite::~Sprite()
{
	if (_vboID != 0)
		glDeleteBuffers(1, &_vboID);
}

void Sprite::init(float x, float y, float width, float height)
{
	_x = x;
	_y = y;
	_width = width;
	_height = height;

	if (_vboID == 0)
		glGenBuffers(1, &_vboID);

	Vertex vertexData[6];

	/*		First Triangle		*/
	// Top Right
	vertexData[0].setPosition(x + width, y + width);
	vertexData[0].setUV(1.0f, 1.0f);
	
	// Top Left
	vertexData[1].setPosition(x, y + height);
	vertexData[1].setUV(0.0f, 1.0f);

	// Bottom Left
	vertexData[2].setPosition(x, y);
	vertexData[2].setUV(0.0f, 0.0f);

	/*		Second Triangle		*/
	// Top Right
	vertexData[3].setPosition(x + width, y + height);
	vertexData[3].setUV(1.0f, 1.0f);

	// Bottom Right
	vertexData[4].setPosition(x + width, y);
	vertexData[4].setUV(1.0f, 0.0f);

	// Bottom Left
	vertexData[5].setPosition(x, y);
	vertexData[5].setUV(0.0f, 0.0f);

	for (int i = 0; i < 6; i++)
	{
		vertexData[i].setColors(200, 100, 0, 255);
	}

	vertexData[1].setColors(255, 0, 255, 255);
	vertexData[4].setColors(200, 0, 50, 255);

	glBindBuffer(GL_ARRAY_BUFFER, _vboID);
	glBufferData(GL_ARRAY_BUFFER,sizeof(vertexData), vertexData, GL_STATIC_DRAW); // Tell OpenGL what is being used
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Sprite::draw()
{
	glBindBuffer(GL_ARRAY_BUFFER, _vboID); // Set the current data

	// Assign our input variables
	// Position Attrib Pointer
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
	glEnableVertexAttribArray(0);
	
	// Color Attrib Pointer
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv));
	glEnableVertexAttribArray(2);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(0);
	//glDisableVertexAttribArray(1);
	//glDisableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
