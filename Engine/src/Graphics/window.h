#pragma once

#include <SDL/SDL.h>
#include "../Input/KeyCode.h"
#include <string>

namespace priv {
	enum class WindowState { RUN, EXIT };
}

namespace engine {
	namespace graphics {

#define NUM_BUTTONS 32

		class Window
		{
		private:
			const char* _name;
			int _width, _height;
			SDL_Window* _window;
			priv::WindowState _windowState;

			// The keys pressed
			bool _keys[KEY_LAST];
			// The mouse buttons pressed
			bool _mouseButtons[NUM_BUTTONS];
			// Mouse x and mouse y
			double _mx, _my;

		public:
			Window(const char* name, int width, int height);
			~Window();
			void close();

			void clear() const;
			void tick();
			bool closed() const;

			bool keyPressed(unsigned int keycode) const;
			bool mouseButtonPressed(unsigned int button) const;
			void mousePosition(double &x, double &y);
			
		private:
			bool init();
			void pollEvent();
			
		};

	} // End namespace graphics
} // End namespace engine