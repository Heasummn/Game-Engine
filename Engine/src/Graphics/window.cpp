#include <iostream>

#include <GL/glew.h>
#include <SOIL/SOIL.h>
#include "Graphics/window.h"

using namespace engine;
using namespace engine::graphics;
using namespace priv;

Window::Window(const char* name, int width, int height) : _name(name), _width(width), _height(height), _window(nullptr), _windowState(WindowState::RUN)
{
	if (!init()) // There was some error in initialization
	{
		SDL_Quit();
	}
		
	// Init keys and mouse buttons
	for (int i = 0; i < KEY_LAST; i++)
	{
		_keys[i] = false;
	}

	for (int i = 0; i < NUM_BUTTONS; i++)
	{
		_mouseButtons[i] = false;
	}

}


Window::~Window()
{
	if (_window != nullptr) // We haven't quit SDL yet
		SDL_DestroyWindow(_window);
	SDL_Quit();
}

void engine::graphics::Window::close()
{
	_windowState = priv::WindowState::EXIT;
}
	
void Window::clear() const
{
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::tick()
{
	pollEvent();
	SDL_GL_SwapWindow(_window);
}

bool Window::closed() const 
{
	return _windowState == WindowState::EXIT;
}

bool Window::keyPressed(unsigned int keycode) const
{
	// TODO: Warning somewhere
	if (keycode >= KEY_LAST)
		return false;
	return _keys[keycode];
}

bool Window::mouseButtonPressed(unsigned int button) const
{
	// TODO: Warning somewhere
	if (button >= NUM_BUTTONS)
		return false;
	return _keys[button];
}

void Window::mousePosition(double &x,double &y)
{
	x = _mx;
	y = _my;
}
	
bool Window::init()
{
	// Initialize SDL and error check
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "Failed to intialize SDL. Stack Trace:\n" << SDL_GetError() << std::endl;
		return false;
	}

	// Create a window
	_window = SDL_CreateWindow(_name, 0, 0, _width, _height, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	if (_window == nullptr) // Assert the window was created
	{
		std::cout << "Could not create Window. Stack Trace:\n" << SDL_GetError() << std::endl;
		return false;
	}

	SDL_GLContext glContext = SDL_GL_CreateContext(_window);
	if (glContext == nullptr)
	{
		std::cout << "GL context could not be created" << std::endl;
		return false;
	}


	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Could to intialize GLEW" << std::endl;
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	return true;
}

void Window::pollEvent()
{
	if (_windowState == priv::WindowState::EXIT)
	{
		SDL_DestroyWindow(_window);
		SDL_Quit();
	}
	SDL_Event evn;
	while (SDL_PollEvent(&evn))
	{
		switch (evn.type)
		{
		case SDL_QUIT:
			_windowState = priv::WindowState::EXIT;
			break;

		case SDL_WINDOWEVENT: // Somthing's happened with the Window
			if (evn.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) // The window has been changed
			{
				SDL_GetWindowSize(_window, &_width, &_height); // Internally change the size
				glViewport(0, 0, _width, _height); // Reset the OpenGL screen to fit the new positions
			}
			break;

		case SDL_KEYDOWN: // Set true when key is pressed
			if(evn.key.keysym.sym <= KEY_LAST) // We want to ignore any modifiers
				_keys[evn.key.keysym.sym] = true;
			break;

		case SDL_KEYUP: // False when key is released
			if(evn.key.keysym.sym <= KEY_LAST)
				_keys[evn.key.keysym.sym] = false;
			break;

		case SDL_MOUSEBUTTONDOWN:
			_mouseButtons[evn.button.button] = true;
			break;
		case SDL_MOUSEBUTTONUP:
			_mouseButtons[evn.button.button] = false;
			break;

		case SDL_MOUSEMOTION:
			_mx = evn.motion.x; // Set x value
			_my = evn.motion.y; // Set y value
			break;
		}
	}

}
