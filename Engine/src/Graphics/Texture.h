#pragma once
#include <GL/glew.h>
namespace engine
{
	namespace graphics
	{
		typedef GLuint TextureID; // A simple typedef to abstract GLuint away
		struct Texture
		{
			TextureID id;
			int width;
			int height;

			void enable()
			{
				glBindTexture(GL_TEXTURE_2D, id);
			}

			void disable()
			{
				glBindTexture(GL_TEXTURE_2D, 0);
			}
		};
	}
}