#pragma once
#include <GL/glew.h>

namespace engine
{
	namespace graphics
	{
		typedef GLubyte colorValue;
		struct Position
		{
			float x, y;
		};

		struct Color
		{
			colorValue r, g, b, a;
		};

		struct UV
		{
			float u, v;
		};

		struct Vertex
		{
			Position position;
			Color color;
			UV uv;

			void setColors(colorValue r, colorValue g, colorValue b, colorValue a)
			{
				color = { r, g, b, a };
			}

			void setUV(float u, float v)
			{
				uv = { u, v };
			}

			void setPosition(float x, float y)
			{
				position = { x, y };
			}
		};
	}
}