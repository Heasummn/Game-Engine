#include <Graphics/window.h>
#include <Graphics/Sprite.h>
#include <Graphics/GLSLProgram.h>
#include <Graphics/Texture.h>
#include <Utils/ImageLoader.h>
#include <GL/glew.h>
#include <iostream>

using namespace engine::graphics;

int main(int argc, char* argv[])
{
	float time = 0;
	Window window("Creative Name Here", 1366, 768);

	glClearColor(0.2f, 0.3f, 0.8f, 1.0f);

	Sprite foo;
	foo.init(-1.0f, -1.0f, 2.0f, 2.0f);

	Texture texture = engine::ImageLoader::loadTexture("resources/Cat.png");

	GLSLProgram colorShader;

	colorShader.compileShaders("Shaders/colorShader.vert", "Shaders/colorShader.frag");
	colorShader.linkShaders();

	while (!window.closed())
	{
		if (window.keyPressed(KEY_ESCAPE))
			window.close();

		window.clear();
		colorShader.use();
		glActiveTexture(GL_TEXTURE0);
		texture.enable();

		time += 0.01f;

		colorShader.setUniform("myTex", 0);

		foo.draw();

		colorShader.unuse();
		texture.disable();
		window.tick();
	}

	return 0;
}