#version 330 core

in vec4 fragmentColor;
in vec2 fragmentPosition;
in vec2 fragmentUV;

layout(location = 0) out vec4 color;

//uniform float time;
uniform sampler2D myTex;

void main()
{

    vec4 texColor = texture(myTex, fragmentUV);

    color = texColor * fragmentColor;
    //color = vec4(fragmentColor.r * (sin(fragmentPosition.x * 4.0 + time) + 1.0) * 0.5,
    //             fragmentColor.b * (cos(fragmentPosition.y * 8.0 + time) + 1.0) * 0.5,
    //             fragmentColor.g * (sin(fragmentPosition.x * 2.0 + time) + 1.0) * 0.5,
    //             1.0);
}